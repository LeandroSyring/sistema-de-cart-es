package com.cartao.mapper;

import com.cartao.models.Cartao;
import com.cartao.models.Pagamento;
import com.cartao.models.dtos.PagamentoRequestDto;

public class PagamentoRequestMapper {

    public static Pagamento fromPagamentoRequestDto (PagamentoRequestDto pagamentoRequestDto){

        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoRequestDto.getDescricao());
        pagamento.setValor(pagamentoRequestDto.getValor());

        Cartao cartao = new Cartao();
        cartao.setId(pagamentoRequestDto.getCartao_id());
        pagamento.setCartao(cartao);
        return pagamento;
    }
}
