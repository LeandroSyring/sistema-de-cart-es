package com.cartao.mapper;

import com.cartao.models.Pagamento;
import com.cartao.models.dtos.PagamentoResponseDto;

public class PagamentoResponseMapper {

    public static PagamentoResponseDto toPagamentoResponseDto(Pagamento pagamento){
        return new PagamentoResponseDto(
                pagamento.getId(),
                pagamento.getCartao().getId(),
                pagamento.getDescricao(),
                pagamento.getValor()
        );
    }
}
