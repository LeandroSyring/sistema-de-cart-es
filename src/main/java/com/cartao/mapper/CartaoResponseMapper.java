package com.cartao.mapper;

import com.cartao.models.Cartao;
import com.cartao.models.dtos.CartaoResponseGetDto;
import com.cartao.models.dtos.CartaoResponsePatchDto;
import com.cartao.models.dtos.CartaoResponsePostDto;

public class CartaoResponseMapper {

    public static CartaoResponseGetDto toCartaoResponseGetDto(Cartao cartao){
        return new CartaoResponseGetDto(
                cartao.getId(),
                cartao.getNumero(),
                cartao.getCliente().getId()
                    );
    }

    public static CartaoResponsePatchDto toCartaoResponsePatchDto(Cartao cartao){
        return new CartaoResponsePatchDto(
                cartao.getId(),
                cartao.getNumero(),
                cartao.getCliente().getId(),
                cartao.isAtivo()
                    );
    }

    public static CartaoResponsePostDto toCartaoResponsePostDto(Cartao cartao){
        return new CartaoResponsePostDto(
                cartao.getId(),
                cartao.getNumero(),
                cartao.getCliente().getId(),
                cartao.isAtivo()
                    );
    }
}
