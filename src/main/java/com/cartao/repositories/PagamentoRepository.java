package com.cartao.repositories;

import com.cartao.models.Cartao;
import com.cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByCartao_id(Integer cartao_id);
}
