package com.cartao.services;

import com.cartao.models.Cartao;
import com.cartao.models.Pagamento;
import com.cartao.models.dtos.PagamentoRequestDto;
import com.cartao.repositories.CartaoRepository;
import com.cartao.repositories.PagamentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    public Pagamento inserirPagamento(Pagamento pagamento){

        Optional<Cartao> cartaoOptional = cartaoService.buscarCartaoPorId(pagamento.getCartao().getId());
        if(cartaoOptional.isPresent()){
            pagamento.setCartao(cartaoOptional.get());
            Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);
            return pagamentoSalvo;
        }
        throw new ObjectNotFoundException(Pagamento.class, "Cartão não encontrado");
    }

    public List<Pagamento> buscarPagamentoPorCartaoId(Integer cartaoId){

        List<Pagamento> pagamentoList = pagamentoRepository.findAllByCartao_id(cartaoId);
        if(!pagamentoList.isEmpty()){
            return pagamentoList;
        }
        throw new ObjectNotFoundException(Pagamento.class, "Pagamento não encontrado");
    }
}
