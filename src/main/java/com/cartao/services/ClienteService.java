package com.cartao.services;

import com.cartao.Exception.ClienteNotFoundException;
import com.cartao.models.Cartao;
import com.cartao.models.Cliente;
import com.cartao.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Optional<Cliente> buscarClientePorID(Integer id){

        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            return  clienteOptional;
        }
        throw new ClienteNotFoundException();
    }
}
