package com.cartao.services;

import com.cartao.Exception.CartaoAlreadyExistsException;
import com.cartao.Exception.CartaoNotFoundException;
import com.cartao.models.Cartao;
import com.cartao.models.Cliente;
import com.cartao.models.dtos.CartaoRequestPostDto;
import com.cartao.repositories.CartaoRepository;
import com.cartao.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteService clienteService;

    public Cartao inserirCartao(Cartao cartao){

        Optional<Cartao> cartaoExistenteOptional = cartaoRepository.findByNumero(cartao.getNumero());
        if(cartaoExistenteOptional.isPresent()){
            throw new CartaoAlreadyExistsException();
        }

        Optional<Cliente> clienteOptional = clienteService.buscarClientePorID(cartao.getCliente().getId());

        cartao.setCliente(clienteOptional.get());

        //Regra de Negócio
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Optional<Cartao> buscarPorNumero(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if(cartaoOptional.isPresent()){
            return cartaoOptional;
        }
        throw new CartaoNotFoundException();
    }

    public Cartao atualizarStatus(String numero, boolean status) {

        try{
            Cartao cartao = this.buscarPorNumero(numero).get();
            cartao.setAtivo(status);
            return cartaoRepository.save(cartao);
        }
        catch (Exception e){
            throw new ObjectNotFoundException(Cartao.class, e.getMessage());
        }

    }

    public Optional<Cartao> buscarCartaoPorId(Integer id){

        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(!cartaoOptional.isPresent()){
            throw new CartaoNotFoundException();
        }
        return cartaoOptional;
    }
}
