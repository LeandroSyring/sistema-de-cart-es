package com.cartao.models.dtos;

public class CartaoResponseGetDto {

    private Integer id;

    private String numero;

    private Integer clienteId;

    public CartaoResponseGetDto(Integer id, String numero, Integer clienteId) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public CartaoResponseGetDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
