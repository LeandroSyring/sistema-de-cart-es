package com.cartao.models.dtos;

public class CartaoRequestPatchDto {

    private boolean ativo;

    public CartaoRequestPatchDto(boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoRequestPatchDto() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
