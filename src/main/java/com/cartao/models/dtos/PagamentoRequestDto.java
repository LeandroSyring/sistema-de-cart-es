package com.cartao.models.dtos;

public class PagamentoRequestDto {

    private Integer cartao_id;

    private String descricao;

    private Double valor;

    public PagamentoRequestDto(Integer cartao_id, String descricao, Double valor) {
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public PagamentoRequestDto() {
    }

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
