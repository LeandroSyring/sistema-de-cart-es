package com.cartao.models.dtos;

public class CartaoRequestPostDto {

    private String numero;

    private Integer clienteId;

    public CartaoRequestPostDto(String numero, Integer clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public CartaoRequestPostDto() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
