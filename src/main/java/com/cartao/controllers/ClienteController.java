package com.cartao.controllers;

import com.cartao.models.Cliente;
import com.cartao.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> inserirCliente(@RequestBody Cliente cliente){
        Cliente clienteSalvo = clienteService.salvarCliente(cliente);
        return ResponseEntity.status(201).body(clienteSalvo);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Cliente>> buscarClientePorID(@PathVariable Integer id){
        Optional<Cliente> clienteOptional = clienteService.buscarClientePorID(id);
        return ResponseEntity.status(200).body(clienteOptional);
    }
}
