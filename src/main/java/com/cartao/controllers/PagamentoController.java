package com.cartao.controllers;

import com.cartao.mapper.PagamentoRequestMapper;
import com.cartao.mapper.PagamentoResponseMapper;
import com.cartao.models.Pagamento;
import com.cartao.models.dtos.PagamentoRequestDto;
import com.cartao.models.dtos.PagamentoResponseDto;
import com.cartao.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<PagamentoResponseDto> inserirPagamento(@RequestBody PagamentoRequestDto pagamentoRequestDto) {

        //Não precisa do try, pois está sendo tratado na service.
        //try {
            Pagamento pagamento = PagamentoRequestMapper.fromPagamentoRequestDto(pagamentoRequestDto);
            Pagamento pagamentoSalvo = pagamentoService.inserirPagamento(pagamento);
            PagamentoResponseDto pagamentoResponseDto = PagamentoResponseMapper.toPagamentoResponseDto(pagamentoSalvo);
            return ResponseEntity.status(201).body(pagamentoResponseDto);
        //} catch (Exception e) {
        //    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        //}
    }

    @GetMapping("/{idCartao}")
    public ResponseEntity<List<PagamentoResponseDto>> buscarPagamentoPorID(@PathVariable Integer idCartao){

        try {
            List<Pagamento> pagamentoList = pagamentoService.buscarPagamentoPorCartaoId(idCartao);
            List<PagamentoResponseDto> pagamentoResponseDtoList = new ArrayList<PagamentoResponseDto>();
            for (Pagamento pag : pagamentoList) {
                PagamentoResponseDto pagamentoResponseDtoItem = PagamentoResponseMapper.toPagamentoResponseDto(pag);
                pagamentoResponseDtoList.add(pagamentoResponseDtoItem);
            }
            return ResponseEntity.status(200).body(pagamentoResponseDtoList);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}
